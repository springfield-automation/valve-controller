export interface ValveController {
  openValve(zone: number): void;
  closeValve(zone: number): void;
}
