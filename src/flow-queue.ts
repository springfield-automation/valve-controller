import { ValveController } from './valve-controller';

export class FlowQueue {
  private valveController: ValveController;
  private requests: Map<number, (() => void)>;

  constructor(valveController: ValveController) {
    this.valveController = valveController;
    this.requests = new Map();
  }

  requestFlow(zone: number) : Promise<void> {
    if(this.requests.has(zone)) {
      throw new Error('Zone already has pending flow request');
    }

    return new Promise((resolve, reject) => {
      this.requests.set(zone, resolve);

      if(this.requests.size == 1) {
        this.valveController.openValve(zone);
        resolve();
      }
    });
  }

  cancelFlow(zone: number) : void {
    if(!this.requests.has(zone)) {
      return;
    }

    const iterator = this.requests.entries();

    if(iterator.next().value[0] == zone) {
      this.valveController.closeValve(zone);

      if(this.requests.size > 1) {
        const next = iterator.next();
        this.valveController.openValve(next.value[0]);
        next.value[1](); // Call the promise resolve()
      }
    }

    this.requests.delete(zone);
  }
}
