import chai from 'chai';
import { FlowQueue } from '../src/flow-queue';
import ValveControllerTestHarness from './valve-controller-test-harness';

chai.should();

describe('water-queue', () => {
  it('should manage a single request', async () => {
    const controller = new ValveControllerTestHarness();
    const queue = new FlowQueue(controller);
    await queue.requestFlow(1);
    controller.getOpenValves().should.deep.equal([1]);
    queue.cancelFlow(1);
    controller.getOpenValves().should.deep.equal([]);
  });

  it('should open one valve at a time when multiple requests', async () => {
    const controller = new ValveControllerTestHarness();
    const queue = new FlowQueue(controller);
    await queue.requestFlow(1);
    const promise = queue.requestFlow(2);
    controller.getOpenValves().should.deep.equal([1]);
    queue.cancelFlow(1);
    await promise;
    controller.getOpenValves().should.deep.equal([2]);
  });
});
