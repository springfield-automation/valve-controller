import { ValveController } from '../src/valve-controller';

export default class ValveControllerTestHarness implements ValveController {
  private openValves: Set<number> = new Set();

  public openValve(zone: number): void {
    this.openValves.add(zone);
  }

  public closeValve(zone: number): void {
    this.openValves.delete(zone);
  }

  public getOpenValves(): number[] {
    return Array.from(this.openValves);
  }
}
